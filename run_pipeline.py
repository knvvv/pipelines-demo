import sys
sys.path.append('./autopy')

from autopy import Transformator, FileItem, ObjectItem, Transform, DataStorage, DirectoryItem, Workflow, run_worker, ThreadManager
from autopy import InstructionFactories as instr
import autopy.transforms.transform_templates as templates
from autopy import TransformStateFactories as ret, TransformState

import json
import shutil

from chemscripts.mylogging import createLogger

# This is the directory for all project-related files.
PROJECT_DIRECTORY = './test_project'

# This is the directory for all temporary files (e.g. garbage produced by Orca or XTB)
# Working directories for all side processes are created here
TEMP_DIRECTORY = './tempfiles'


def main(main_logger):
    # The pipeline implemented here does the following ("pretends" to do just to demonstrate the concept of pipelines):
    # 1) Input data are starting coordinates of some molecules
    # 2) Save these starting coordinates to XYZ file ('save_starting_xyz_transform')
    # 3) Pretend to do some confsearch to create separate XYZ files for conformers ('do_some_confsearch')
    # 4) Pretend to run Orca on each conformer to generate file with optimized geometry and energies ('optimize_in_orca')
    # 5) Load data from Orca logs as Python objects and copy the log of the lowest energy conformer into separate directory
    # 6) Open the copied lowest energy logs and extract energies

    #
    # DEFINE DATAITEMS - THE DATA WE ARE GOING TO WORK WITH
    #
    ds = DataStorage({
            # 'type': 'object' means that 'start_geometries' are Python-objects
            # Many such 'start_geometries' objects can exist, but 
            # for each object there is a unique combination of values for 'calcname' and 'molname' keys
            # e.g. {calcname: "Ozonolysis", "molname": "PrimaryOzonide"}
            'start_geometries': {'type': 'object', 'keys': ['calcname', 'molname']},

            # These are files and they are stored at PROJECT_DIRECTORY
            # All paths have some keys as f-strings ("{keyA}_{keyB}"). The values of these keys are
            # parsed during startup and automatically files are saved with given keys ()
            'start_xyzs': {'type': 'file', 'mask': './{calcname}_start/{molname}.xyz'},
            'conformer_xyzs': {'type': 'file', 'mask': './{calcname}_conformers/{molname}_{confidx}.xyz'},
            'orca_logs': {'type': 'file', 'mask': './{calcname}_optimized/{molname}/conf_{confidx}.out'},
            'optimized_conformers': {'type': 'object', 'keys': ['calcname', 'molname', 'confidx']},
            'best_conformers': {'type': 'file', 'mask': './{calcname}_best/{molname}.out'},
            'best_energies': {'type': 'object', 'keys': ['calcname', 'molname']},
        }, # Each line above will become a DataItem
        allow_overwrite=True,
        instantiate_prototypes=True,
        wd=PROJECT_DIRECTORY
    )

    # Remove all files from PROJECT_DIRECTORY
    # Pipelines won't go from the beginning (start_geometries) if the required files already exist
    ds.conformer_xyzs.cleanup()
    ds.orca_logs.cleanup()
    ds.best_conformers.cleanup()

    # but we can't run pipeline having no data at all. So add a few starting geometries:
    ds.start_geometries.include_element({
        'coords': 'Geometry of ethylene',
        'charge': 0,
    }, calcname='Ozonolysis', molname='Ethylene')

    ds.start_geometries.include_element({
        'coords': 'Geometry of ozone',
        'charge': 0,
    }, calcname='Ozonolysis', molname='Ozone')

    ds.start_geometries.include_element({
        'coords': 'Geometry of ozonide',
        'charge': 0,
    }, calcname='Ozonolysis', molname='Ozonide')
    # imagine that "Geometry of ethylene" and others are in fact np.arrays of XYZ coordinates


    #
    # DEFINE TRANSFORMS - THE BUILDING BLOCKS OF THE FUTURE PIPELINE
    #

    # Transform is, essentially, a simple function that knows how to do one specific thing
    # The less it knows about structure of the outside project, the better
    def save_starting_xyz_exec(start_geometries: ObjectItem, start_xyzs: FileItem, logger) -> TransformState:
        logger.info("'save_starting_xyz' has no idea about the two keys ('calcname' 'molname') of the input and output objects."
              "But it knows just enough to transform one into another")
        
        current_molecule: dict = start_geometries.access_element()
        xyz_filename: str = start_xyzs.get_path()
        logger.info(f"Got {current_molecule}. Now going to save it to some file. "
                    f"Literally couldn't care less about it's name ({xyz_filename}), "
                    f"or whether its containing directory exists (it's guaranteed to be created as soon as start_xyzs.get_path() is called)")
        
        with open(xyz_filename, 'w') as f:
            json.dump(current_molecule, f)
        
        start_xyzs.include_element(xyz_filename)
        # The new element of 'start_xyzs' inherits values for 'calcname' and 'molname' from the starting start_geometries object
        # and still this function does not have to do anything

        # Generally, transform can have multiple methods (this one has only 'save_starting_xyz_exec') which can be called multiple times,
        # so we need to say that this transform is finished and to callback is not needed
        return ret.transform_finished()

    # Now, the save_starting_xyz_exec function has to be combined
    # with additional info about source and target DataItems
    save_starting_xyz_transform = Transform(
        NAME = 'save_starting_xyz_transform',
        SOURCE_ITEMS = ['start_geometries'],
        TARGET_ITEMS = ['start_xyzs'],
    ).set_method({'exec': save_starting_xyz_exec})
    # ... and this is how you define a transform! See below how it is used further

    # Now, define functions for other transforms
    def generate_conformers(
            start_xyzs: FileItem, # Starting geometry file
            conformer_xyzs: FileItem, # Files of resulting conformers (there will be several of them!)

            # To demonstrate the concept of aware keys, we specified aware_keys=['molname'] (see below)
            molname: str # This transform knows which molecule it's working on (it is aware of the 'molname' key)
        ) -> None:

        # Usual sequence: input.access_element() => do some work => output.get_path() => save file => output.include_element(file_path)

        with open(start_xyzs.access_element(), 'r') as f:
            start_object = json.load(f)
        
        # Pretend to generate conformers
        for confidx in range(5):
            # NOTE: Here, we are doing .get_path() and .include_element() for conformer_xyzs by specifying 'confidx' value, because
            # this transform generates this key ('start_xyzs' does not have 'confidx' key and 'conformer_xyzs' does have it)
            # Thus, it has to be the job of this transform to generate the value of confidx for each new file
            conformation_path = conformer_xyzs.get_path(confidx=confidx)
            with open(conformation_path, 'w') as f:
                json.dump({
                    **start_object,
                    'coords': f'Conformer #{confidx} of {molname}'
                }, f)
            conformer_xyzs.include_element(conformation_path, confidx=confidx)
        # 'return ret.transform_finished()' is not required when using 'templates.exec()'

    def load_optimized_geometries(
            # These two input items are grouped at their 'confidx' keys, because merged_keys=['confidx'] was specified
            conformer_xyzs: FileItem, # Starting files for Orca optimizations (using them here just to show that I can)
            orca_logs: FileItem, # Orca logs for processing

            # These two are items generated by this transform:
            # 1) Python objects obtained after parsing of Orca logs (multiple conformers - multiple element)
            optimized_conformers: ObjectItem,
            # 2) Copy of the log of the best conformer in a separate directory. There is only one best conformer for each molecule
            best_conformers: FileItem
        ) -> None:
        
        min_energy = None
        best_conformer_log = None

        # These are multiple elements in orca_logs with different 'confidx' values
        # Can access these element iteratively using 'for'
        for log_name, keys in orca_logs:
            # keys is something like {'confidx': 1}

            # Need to access input file correspoding to the same 'confidx' value - just pass **keys
            starting_xyz = conformer_xyzs.access_element(**keys)
            print(f"Done optimization {starting_xyz} => {log_name}")

            with open(log_name, 'r') as f:
                optimized_mol = json.load(f)
            # optimized_conformers also has 'confidx' key, so pass it as **keys when including element
            optimized_conformers.include_element(optimized_mol, **keys)

            if min_energy is None or min_energy > optimized_mol['energy']:
                best_conformer_log = log_name
                min_energy = optimized_mol['energy']
        
        best_conformer_path = best_conformers.get_path()
        shutil.copy2(best_conformer_log, best_conformer_path)
        # best_conformers does not expect any additional key since it does not have 'confidx' (one best conformer per molecule)
        best_conformers.include_element(best_conformer_path)


    #
    # COMBINE TRANSFORMS SO THEY ARE CALLED AUTOMATICALLY 
    #

    # Transformator is a class that calls transforms to reach the required DataItem. It does something like this:
    # "Okay, I need to obtain C, but I have only A. Also, I have transforms A->B and B->C"
    # "I don't have B, so I am calling A->B first and, then, B->C"
    main_transformator = Transformator(transformations=[
            # Just list all transforms that you have in any order
            save_starting_xyz_transform,
            
            # For most typical operations with data, you don't need to create a Transform object using Transform() constructor
            # In majority of cases, there probably exists an appropriate template (transform factory)
            templates.exec('do_some_confsearch',
                input='start_xyzs', output='conformer_xyzs', method=generate_conformers, aware_keys=['molname']
            ),
            templates.nonblocking_subprocess(name='optimize_in_orca',
                input='conformer_xyzs', output='orca_logs',
                command_prepare=lambda conformer_xyzs, orca_logs, calcdir, **kw: f"python orca.py {conformer_xyzs.access_element()} {orca_logs.get_path()} {calcdir}",
                # Use lambda for 'output_process' just to avoid writing a usual function
                # It returns None, but it's not important (include_element is needed to be called)
                output_process=lambda orca_logs, **kw: orca_logs.include_element(orca_logs.get_path()),
                calcdir='calcdir', nproc=1
            ),
            templates.exec(name='load_optimized_geometries',
                input=['conformer_xyzs', 'orca_logs'], output=['optimized_conformers', 'best_conformers'], merged_keys=['confidx'],
                method=load_optimized_geometries
            ),
            templates.map('extract_energies',
                input='best_conformers', output='best_energies',
                mapping=lambda best_conformers, **kw: json.load(open(best_conformers, 'r'))['energy']
            )
        ],
        storage=ds,
        logger=main_logger,
    )

    # So you see that keys of DataItems are at the heart of pipeline automation.
    # Let's summarise: at each transform involves one or several source and target DataItems;
    # each DataItem has its own set of keys. Thus, pipeline framework decides the "mode" in which
    # it is going to process every key. There are 4 modes of processing: 
    # 1) Unaware key is the nicest mode (when all involved items have this key), then the transform function
    #    does not even have to know whether items have this key. Automated iteration over all possible value combinations.
    #    Don't have to specify the key value when including element.
    # 2) Aware key (some of source items don't have the key). Automated iteration over all possible value combinations, but
    #    transform function accepts the value of aware key on each iteration. Don't have to specify the key value when including element.
    # 3) Generated key (no source items have the key, but some targets do). Have to specify the key value when including element.
    # 4) Merged key (some of target items don't have the key). Iteration over all possible value combinations must be
    #    implemented manually inside the transform function. Have to specify the key value when including element.


    #
    # WORKFLOW SETUP
    #

    # 'thread_manager' is responsible for executing tasks in separate subprocesses
    # Also, it allocates temporary directories for them f"{TEMP_DIRECTORY}/{random_string}/"
    thread_manager = ThreadManager(wd='./tempfiles', maxproc=16)
    
    # Workflow object knows about all transformators that are defined in the program
    workflow = Workflow(
        transformators={
            'main': main_transformator,
        },
        storage=ds,
        logger=main_logger,
    )

    # The actual program execution starts here
    workflow.execute([
        # This is the call for our only transformator to generate the final target 'best_energies'
        # It will start from 'start_geometries' (the only DataItem that is currently non-empty)
        instr.reach_target(transformator='main', target='best_energies'),
    ], forward={ # These objects will be forwarded to all transforms
                 # 'logger' to log messages (see above),
                 # thread_manager - to execute subprocesses (see implementation of 'templates.nonblocking_subprocess')
        'logger': main_logger,
        'thread_manager': thread_manager
    })

    # At this point, all "calculations" have to be finished and DataItem 'best_energies' must be available
    
    for energy, keys in ds.best_energies:
        # You see that DataStorage and DataItems are pretty useful even outside of the pipeline framework
        print(f"We have the energy={energy} for {keys}")

    ds.orca_logs.cleanup() # Delete orca logs. They are always heavy af

    main_logger.warning('DONE')


if __name__ == "__main__":
    logger = createLogger("Main")
    main(logger)
