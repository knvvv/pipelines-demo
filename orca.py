import sys
import json
import random


if __name__ == "__main__":
    input, output, calcdir = sys.argv[1], sys.argv[2], sys.argv[3]
    print(f"Can create as many files as I like at {calcdir} - tempdir that was allocated specifically for this process")

    with open(input, 'r') as file:
        mol_data = json.load(file)
    
    # Pretent to optimize geometry
    mol_data['coords'] = f"Optimized {mol_data['coords']}"
    mol_data['energy'] = random.uniform(0.0, 1.0)

    with open(output, 'w') as f:
        json.dump(mol_data, f)
