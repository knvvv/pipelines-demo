# Pipeline automation in Python. A simple demonstration

This example demonstrates why pipeline programming is superior to traditional approach, specifically, for scientific computing and data processing when the algorithm includes multiple executable calls, data and file transformation steps, asynchronous/remote jobs, etc.


## Execute the demo

```
git clone --recursive https://gitlab.com/knvvv/pipelines-demo.git
conda env create -n pipe-demo -f autopy/environment.yaml
conda activate pipe-demo
python run_pipeline.py
```

## Summary

This pipeline automation framework provides several advantages:

1) Program is consists of small blocks of code (transforms) that work on fixed source and target data. Transforms can be implemented anonymously - the code does not have to care about the context of the project, figure out file paths, directories, places for temporary files, etc. This makes one single transform highly reusable across several tasks/projects. Several inheritance paradigms can be used to derive concrete transforms from more abstract ones, which results in even more reusable code.

2) Transforms, transformators and workflows make the flow of data in the program quite predictable, so execution of some code block can be (potentially) automatically parallized, delegated to remote servers without the programmer having to do anything. Execution of transforms is already planned automatically, so pipeline frameworks automatically skips execution of some computations if, for example, the resulting log-files are already present in project directory.

3) The concept of dataitems and keys allows to separate the implementation of data structure of the project (which files are stored where, how do they relate to python objects) and transforms of the data items. Data storage can be reorganized without having to change anything in the algorithm itself and vice versa.
